file = open('dnevnik.txt', 'r')
table_file = open('new_dnevnik.txt', 'w')
column_one = 'Date'
column_two = 'Job'
column_one_len = 18
column_two_len = 59
length = 80


def check_line(text: str, elem: str, size: int):
    text += ' ' + elem[0:(size - len(text) - 2)] + \
            (
                '-|\n' + '|' + write_new_line('', date=True) + '|' +
                (
                    check_line(text, elem[(size - len(text) - 3): len(elem)], size)
                    if (size - len(text) - 3) < len(elem)
                    else
                    ''
                )
                if len(elem[0:(size - len(text))]) >= size
                else
                (' ' * (size - len(elem[0:(size - len(text))]) - 1))
            )
    return text


def write_new_line(text: str, date=False):
    if date:
        return (' ' * ((column_one_len - len(text)) // 2)) + \
               text + (' ' * ((column_one_len - len(text)) // 2))
    else:
        if len(text) <= column_two_len:
            return (' ' * (((column_two_len - len(text)) // 2) + (1 if len(text) % 2 == 0 else 0))) + \
                   text.replace('\n', ' ') + (' ' * ((column_two_len - len(text)) // 2))
        else:
            string = ''
            for elem in text.split(' '):
                if len(elem) > column_two_len:
                    string = check_line(string, elem, column_two_len)
                elif len(string + elem) <= column_two_len:
                    string += elem.strip()
                else:
                    string += ' ' * (column_two_len - len(string)) + '|\n' + '|' + \
                              write_new_line('', date=True) + '|' + elem
            return string


table_file.write(('_' * length) + '\n' + '|' + write_new_line(column_one, date=True) + '|' +
                 write_new_line(column_two) + '|' + '\n' + ('_' * length) + '\n')
for line in file:
    table_file.write('|' + write_new_line('', date=True) + '|' + write_new_line('') + '|' + '\n')
    table_file.write('|' + write_new_line(line.split(' ')[0] + ' ' + line.split(' ')[1], date=True) + '|'
                     + write_new_line(line.replace(line.split(' ')[0] + ' ' + line.split(' ')[1], '')) + '|' + '\n')
    table_file.write('|' + write_new_line('', date=True) + '|' + write_new_line('') + '|' + '\n')
    table_file.write('_' * length + '\n')
