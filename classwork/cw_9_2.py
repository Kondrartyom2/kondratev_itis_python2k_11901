import requests
import bs4


def download(itr: int, element: str):
    with open('Img{}.{}'.format(itr, element.split('.')[-1]), 'wb') as file:
        file.write(requests.get(element).content)
        file.close()


try:
    response = requests.get('{}'.format(input("Введите ссылку: ")))
    i = 0
    for elements in bs4.BeautifulSoup(response.text, 'html.parser').select('img')[0:5]:
        elem = elements.get('src')
        if elem.startswith('https://' or 'http://'):
            download(i, elem)
        i += 1
except requests.exceptions.ConnectionError:
    print("Ошибка")
