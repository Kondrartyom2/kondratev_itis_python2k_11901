import threading
import multiprocessing
import bs4
import requests


def download(image):
    with open('Img{}.{}'.format(image[0], image[1].split('.')[-1]), 'wb') as file:
        file.write(requests.get(image[1]).content)
        file.close()


if __name__ == '__main__':
    images = []
    response = requests.get('{}'.format(input("Введите ссылку: ")))
    for elements in bs4.BeautifulSoup(response.text, 'html.parser').select('img'):
        elem = elements.get('src')
        if elem.startswith('https://' or 'http://') and (elem.endswith('.png') or elem.endswith('.jpg')):
            images.append(elem)
    with multiprocessing.Pool() as pool:
        res = pool.map(download, enumerate(images))


