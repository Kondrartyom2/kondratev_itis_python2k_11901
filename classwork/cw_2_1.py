string = str(input())
answer = {}


def sigma(s: str, case_sens=True):
    if not case_sens:
        s = s.lower()
    for j in s:
        if j not in answer:
            answer[j] = 1
        else:
            answer[j] += 1
  

if __name__ == '__main__':
    sigma(string, True)
    sorted(answer)
    for key in answer:
        print(key, ': ', answer[key])
