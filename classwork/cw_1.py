string = input()
sum = 0
for j in string.strip().lower():
    if j in ['a', 'e', 'i', 'o', 'u', 'y']:
        sum += 1
print(sum)