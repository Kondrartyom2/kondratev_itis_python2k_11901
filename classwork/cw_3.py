class Vector2D:
    x = 0.0
    y = 0.0

    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def __add__(self, vector):
        self.x += vector.x
        self.y += vector.y

    def add2(self, x=0.0, y=0.0, vector=None):
        return Vector2D(self.x + x if vector is None else vector.x, self.y + y if vector is None else vector.y)

    def __sub__(self, vector):
        self.x -= vector.x
        self.y -= vector.y

    def sub2(self, x=0.0, y=0.0, vector=None):
        return Vector2D(self.x - x if vector is None else vector.x, self.y - y if vector is None else vector.y)

    def __mul__(self, vector) -> float:
        return (self.x * vector.x) + (self.y * vector.y)

    def __str__(self):
        return '({}, {})'.format(self.x, self.y)
