import urllib.request
import re
import os
import requests

link = urllib.request.urlopen('file:///C:/Users/Artem%20Work/Desktop/InfoCourses/Python/Python/Lesson8/files.html')


def find_url(line: str, teg: str):
    return line.replace('b', '').replace('\'', '').replace('<{}>'.format(teg), '') \
        .replace('</{}>'.format(teg), '').split('\\')[0]


def download(url: str):
    os.chdir('C:\\Users\\Artem Work\\Desktop\\InfoCourses\\Python\\Python\\Lesson8')
    try:
        if re.match(r'https?://[\w.-]+', url) is not None:
            file = open(os.path.abspath('') + '\\' + url.split('/')[-1], 'wb')
            file.write(requests.get(url).content)
            file.close()
    except ConnectionError:
        print(ConnectionError)


links = []
teg = input("Введите тег которым разделены ссылки -> ")
for line in link.readlines():
    if str(line).find('<{}>'.format(teg)) != -1:
        download(find_url(str(line), teg))

link.close()