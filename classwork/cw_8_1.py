import re

regular_of_function = re.compile("\.((\w+)(?P<arguments>\((([^0-9](\w)+)( ([^\d](\w)*)($|(, )?))*)*\)))")
regular_of_integer_param = re.compile("\(((Integer|int)( ([^\d](\w)*)($|(, )?))*)*\)")
sum_of_function = 0
sum_of_integer = 0
for line in open('code.txt'):
    try:
        for function in regular_of_function.findall(line):
            sum_of_integer += len(regular_of_integer_param.findall(function[0]))
        sum_of_function += len(regular_of_function.findall(line))
        print(regular_of_function.search(line).group())
    except AttributeError:
        sum_of_function += 0
print("Всего вызванных функций {}".format(sum_of_function)) #a
print("Всего функций с int {}".format(sum_of_integer)) #b
