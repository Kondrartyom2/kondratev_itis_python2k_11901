import datetime


def convert(seconds):
    return int(seconds // 3600), int((seconds // 60) % 60)


timetable = open('timetable.txt', 'r')
days_less = {'Monday': 0.0, 'Tuesday': 0.0, 'Wednesday': 0.0, 'Thursday': 0.0,
             'Friday': 0.0, 'Saturday': 0.0, 'Sunday': 0.0}
lesson_dic = {}
answer_1 = {'lesson_time': 0.0, 'brake_time': 0.0}
cur_day = ''
cur_less_time = None

for line in timetable:
    if line.strip() in days_less.keys():
        cur_day = line.strip()
        continue
    else:
        if cur_less_time is not None:
            answer_1['brake_time'] += (datetime.datetime(year=2020, month=10, day=1,
                                                         hour=int(line.split(' ')[0].split('-')[0].split(':')[0]),
                                                         minute=int(line.split(' ')[0].split('-')[0].split(':')[1])) -
                                       cur_less_time) \
                .seconds
        cur_less_time = datetime.datetime(year=2020, month=10, day=1,
                                          hour=int(line.split(' ')[0].split('-')[1].split(':')[0]),
                                          minute=int(line.split(' ')[0].split('-')[1].split(':')[1]))
        time = datetime.datetime(year=2020, month=10, day=1,
                                 hour=int(line.split(' ')[0].split('-')[1].split(':')[0]),
                                 minute=int(line.split(' ')[0].split('-')[1].split(':')[1])) - \
               datetime.datetime(year=2020, month=10, day=1,
                                 hour=int(line.split(' ')[0].split('-')[0].split(':')[0]),
                                 minute=int(line.split(' ')[0].split('-')[0].split(':')[1]))
        if line.split(' ')[1].strip() in lesson_dic.keys():
            lesson_dic[line.split(' ')[1].strip()] += time.seconds
        else:
            lesson_dic[line.split(' ')[1].strip()] = time.seconds
        days_less[cur_day] += time.seconds
        answer_1['lesson_time'] += time.seconds

# for line in timetable:
#     if line.strip() in days_less.keys():
#         cur_day = line.strip()
#         continue
#     else:
#         if line.split(' ')[1].strip() not in lesson_dic.keys():
#             lesson_dic[line.split(' ')[1].strip()] = 0
#         cur_time = (datetime.datetime(year=2020, month=5, day=1,
#                                      hour=int(line.split(' ')[0].split('-')[1].split(':')[0]),
#                                      minute=int(line.split(' ')[0].split('-')[1].split(':')[1])) -\
#                    datetime.datetime(year=2020, month=5, day=1,
#                                      hour=int(line.split(' ')[0].split('-')[0].split(':')[0]),
#                                      minute=int(line.split(' ')[0].split('-')[0].split(':')[1]))).total_seconds()
#         lesson_dic[line.split(' ')[1].strip()] += cur_time
#         days_less[cur_day] += cur_time
#         lesson_time += cur_time
#

print('Task1')
hours, minutes = convert(answer_1['lesson_time'])
print('Lesson time : ' + "{}:{}".format(hours, minutes))
hours, minutes = convert(answer_1['brake_time'])
print('Brake time : ' + "{}:{}".format(hours, minutes))
print('Task2')
for day in days_less.keys():
    print(day)
    hours, minutes = convert(days_less[day])
    print("{}:{}".format(hours, minutes))
print('Task3')
for lesson in lesson_dic.keys():
    print(lesson)
    hours, minutes = convert(lesson_dic[lesson])
    print("{}:{}".format(hours, minutes))
