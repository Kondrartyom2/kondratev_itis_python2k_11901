import datetime
import os
import re
import shutil
import cv2
import requests

os.chdir('C:\\Users\\Artem Work\\учеба\\info')


def viewImage(image, name_of_window):
    cv2.namedWindow(name_of_window, cv2.WINDOW_NORMAL)
    cv2.imshow(name_of_window, image)
    cv2.waitKey(0)


def move(what: str, where: str):
    try:
        shutil.move(what, where)
    except shutil.Error:
        print('Ошибка')


def sort(day=False, month=False, year=False):
    for element in os.listdir(os.path.abspath("")):
        if element.split('.')[-1] == 'jpg' or 'png' or 'img':
            name = datetime.datetime.fromtimestamp(os.stat(os.path.abspath('') + '\\' + element)
                                                   .st_ctime).date().__str__().replace('-', '')
            if month:
                name = datetime.datetime.fromtimestamp(os.stat(os.path.abspath('') + '\\' + element)
                                                       .st_ctime).date().__str__() \
                    .replace(datetime.datetime.fromtimestamp(os.stat(os.path.abspath('') + '\\' + element)
                                                             .st_ctime).date().__str__().split('-')[-1], '') \
                    .replace('-', '')
            elif year:
                name = datetime.datetime.fromtimestamp(os.stat(os.path.abspath('') + '\\' + element)
                                                       .st_ctime).date().__str__().split('-')[0]
            if name not in os.listdir(os.path.abspath("")):
                os.mkdir(name)
                move(os.path.abspath('') + '\\' + element, os.path.abspath('') + '\\' + name)
            else:
                move(os.path.abspath('') + '\\' + element, os.path.abspath('') + '\\' + name)


while True:
    command = str(input(os.path.abspath("") + '>'))
    try:
        # +
        if command.strip() == 'exit':
            break
        # +

        elif command.strip() == 'dir':
            print('Содержимое папки ' + os.path.abspath("") + '\n')
            for element in os.listdir(os.path.abspath("")):
                print(element)

        elif command.startswith('cd'):
            # +
            if command.strip() == 'cd':
                print(os.path.abspath(""))
            # +
            elif command.strip() == 'cd..' or command.split(' ')[1] == '..':
                os.chdir(os.path.abspath("").replace(os.path.abspath("").split('\\')[-1], ''))
            else:
                try:
                    os.chdir(command.replace('cd ', ''))
                except FileNotFoundError:
                    print('Системе не удается найти указанный путь.')

        elif command.startswith('open '):
            try:
                if command.replace('open ', '').endswith('.txt'):
                    for line in open(os.path.abspath('') + '\\' + command.split('open ')[1], 'r'):
                        print(line, end='')
                    print()
                elif command.replace('open ', '').endswith('.png') or \
                        command.replace('open ', '').endswith('.jpeg') or \
                        command.replace('open ', '').endswith('.img') or \
                        command.replace('open ', '').endswith('.jpg'):
                    # image = cv2.cvtColor(cv2.imread(os.path.abspath('') + '\\' + command.replace('open ', '')),
                    #                      cv2.COLOR_BGR2RGB)
                    print(os.path.abspath('') + '\\' + command.replace('open ', ''))
                    image = cv2.imread('Audi.png', 1)
                    while True:
                        print('Выберите что вы хотите сделать с изображением: {}'.format(command.replace('open ', ''))
                              + '\n Команды: '
                                '\n bw - для того чтоб получить чб '
                                '\n show - вывести изображение '
                                '\n exit - назад'
                              )
                        img_command = str(input(os.path.abspath("") + '>'))
                        if img_command.strip() == 'exit':
                            break
                        elif img_command.strip() == 'show':
                            print(image)
                            cv2.imshow("my", image)

                else:
                    print('Это не текстовый файл')
            except FileNotFoundError:
                print('Не удается найти указанный файл: ' + os.path.abspath('') + '\\' + command.split('open ')[1])
            except cv2.error:
                print('Eror: ' + cv2.error.err)

        elif command.startswith('delete '):
            try:
                try:
                    os.rmdir(os.path.abspath('') + '\\' + command.split('delete ')[1])
                except NotADirectoryError:
                    os.remove(os.path.abspath('') + '\\' + command.split('delete ')[1])
            except FileNotFoundError:
                print('Не удается найти указанный файл: ' + os.path.abspath('') + '\\' + command.split('delete ')[1])
            except NotADirectoryError:
                print('Неверно задано имя папки: ' + os.path.abspath('') + '\\' + command.split('delete ')[1])

        elif command.startswith('move '):
            move(os.path.abspath('') + '\\' + command.replace('move ', '').split(' ')[0],
                 command.replace('move ' + command.replace('move ', '').split(' ')[0], '').strip())

        elif command.startswith('copy '):
            try:
                shutil.copy(os.path.abspath('') + '\\' + command.replace('copy ', '').split(' ')[0],
                            command.replace('copy ' + command.replace('copy ', '').split(' ')[0], '').strip())
            except shutil.Error:
                print('Ошибка')

        elif command.startswith('download '):
            try:
                if re.match(r'https?://[\w.-]+', command.split(' ')[1]) is not None:
                    file = open(os.path.abspath('') + '\\' + command.split(' ')[1].split('/')[-1], 'wb')
                    file.write(requests.get(command.split(' ')[1]).content)
                    file.close()

            except ConnectionError:
                print('"{}" является некоректным \n url, следовательно не может быть открыт.'
                      .format(command.split(' ')[1]))

        elif command.startswith('sort_by '):
            if command.split(' ')[1] == 'day':
                sort(day=True)
            elif command.split(' ')[1] == 'month':
                sort(month=True)
            elif command.split(' ')[1] == 'year':
                sort(year=True)
            else:
                print('"{}" не является внутренней или внешней \n командой, для сортировки файлов.'
                      .format(command.split(' ')[1]))

        else:
            print('"{}" не является внутренней или внешней \n командой, исполняемой программой или пакетным файлом.'
                  .format(command))

    except IndexError:
        print('"{}" не является внутренней или внешней \n командой, исполняемой программой или пакетным файлом.'
              .format(command))
    except OSError:
        print(OSError)
