
def normal_url(domen :str, url: str):
    if url.startswith('https://{}'.format(domen)):
        return url
    elif url.startswith(domen):
        return 'https://{}'.format(url)
    elif url.startswith('//{}'.format(domen)):
        return 'https:{}'.format(url)
    else:
        return 'https://{}{}'.format(domen, url)


print(normal_url('http.cat', '200.jpg'))


