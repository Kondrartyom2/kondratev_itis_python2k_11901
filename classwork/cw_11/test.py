from Lesson11.utils import normal_url


def test__url_first_case():
    assert normal_url('http.cat', 'https://http.cat/200.jpg') == 'https://http.cat/200.jpg'


def test__url_second_case():
    assert normal_url('http.cat', '//http.cat/200.jpg') == 'https://http.cat/200.jpg'


def test__url_third_case():
    assert normal_url('http.cat', '/200.jpg') == 'https://http.cat/200.jpg'


def test__url_fourth_case():
    assert normal_url('http.cat', 'http.cat/200.jpg') == 'https://http.cat/200.jpg'


def test__url_dead_test():
    assert normal_url('http.cat', '200.jpg') != 'https://http.cat/200.jpg'