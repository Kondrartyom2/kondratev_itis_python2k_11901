words = {}
right_file = open('lower.txt', 'w')
index = 0
words['\n'] = 0
index += 1


def get_key(dic, value):
    for key, val in dic.items():
        if val == value:
            return key


if __name__ == "__main__":
    for line in open('normal.txt', 'r'):
        for word in line.split(' '):
            if word in words.keys():
                right_file.write('{} '.format(words[word]))
            else:
                words[word] = index
                right_file.write('{} '.format(words[word]))
                index += 1
        right_file.write('{} '.format(words['\n']))
    right_file = open('lower.txt', 'r')
    for line in right_file:
        for num in line.split(' '):
            try:
                print(get_key(words, int(num)), end=' ')
            except ValueError:
                print()
