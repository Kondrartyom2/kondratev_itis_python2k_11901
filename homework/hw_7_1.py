import datetime
from xml.etree.ElementTree import ElementTree

from lxml import etree
import requests

response = requests.get('http://tass.ru/rss/v2.xml')
root: ElementTree = etree.XML(response.text.encode())

channel = root.find('channel')
for item in channel.iterfind('item'):
    item: ElementTree
    title = item.find('title').text if item.find('title') is not None else ''
    pubDate = item.find('pubDate').text.split(' ') if item.find('pubDate') is not None else ''
    sub = item.find('description').text if item.find('description') is not None else ''
    date = datetime.datetime.strptime(pubDate[1] + ' ' + pubDate[2] + ' ' + pubDate[3], '%d %b %Y')
    print(title)
    print(sub)
    print(str(date.day) + '.' + str(date.month) + '.' + str(date.year))
