def algorithm(greek_num: dict, num: int):
    answer = ''
    while num > 0:
        if (num / 1000) >= 1:
            num -= 1000
            answer += greek_num[1000]
        elif (num / 500) >= 1:
            num -= 500
            answer += greek_num[500]
        elif (num / 100) >= 1:
            num -= 100
            answer += greek_num[100]
        elif (num / 50) >= 1:
            num -= 50
            answer += greek_num[50]
        elif (num / 10) >= 1:
            num -= 10
            answer += greek_num[10]
        elif (num / 5) >= 1:
            num -= 5
            answer += greek_num[5]
        else:
            num -= 1
            answer += greek_num[1]
    return answer


def greek(func):
    greek_num = {1: 'I', 5: 'V', 10: 'X', 50: 'L', 100: 'C', 500: 'D', 1000: 'M'}


    def work(num: int):
        return algorithm(greek_num, num)
    return work


@greek
def number(num: int):
    return num


print(number(int(input())))