class List(list):
    type = type
    list = list()

    def __init__(self, type: type):
        self.type = type

    def __str__(self):
        return self.list.__str__()

    def check(self, obj: object):
        return True if issubclass(self.type, type(obj)) else False

    def append(self, obj):
        if self.check(obj):
            self.list.append(obj)
        else:
            TypeError

    def insert(self, __index: int, __object) -> None:
        if self.check(__object):
            self.list.insert(__index, __object)
        else:
            TypeError
