import pygame

# STATIC VARIABLES
WEIDTH = 500
HEIGHT = 300
FPS = 50
# COLORS
aqua = (0, 255, 255)  # морская волна
black = (0, 0, 0)  # черный
blue = (0, 0, 255)  # синий
fuchsia = (255, 0, 255)  # фуксия
gray = (128, 128, 128)  # серый
green = (0, 128, 0)  # зеленый
lime = (0, 255, 0)  # цвет лайма
maroon = (128, 0, 0)  # темно-бордовый
navy_blue = (0, 0, 128)  # темно-синий
olive = (128, 128, 0)  # оливковый
purple = (128, 0, 128)  # фиолетовый
red = (255, 0, 0)  # красный
silver = (192, 192, 192)  # серебряный
teal = (0, 128, 128)  # зелено-голубой
white = (255, 255, 255)  # белый
yellow = (255, 255, 0)  # желтый
collor_coef = 2.55
# BAR SIZE
x_coordinate = 50
y_coordinate = 125
bar_weidth = 4
bar_height = 50
downloading_speed = 4
max_size = 400
bar_collor = (255, 0, 0)
min_size = bar_weidth
# TEXT SETTINGS
text_size = 30

# CREATE GAME START FUNCTIONS
pygame.init()
screen = pygame.display.set_mode((WEIDTH, HEIGHT))
pygame.display.set_caption("Download")
clock = pygame.time.Clock()
running = True

while running:
    clock.tick(FPS)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    button = pygame.key.get_pressed()
    if button[pygame.K_LEFT] and bar_weidth > min_size:
        bar_weidth -= downloading_speed
        bar_collor = ((bar_collor[0] + collor_coef),
                      (bar_collor[1] - collor_coef), bar_collor[2])
    if button[pygame.K_RIGHT] and bar_weidth < max_size:
        bar_weidth += downloading_speed
        bar_collor = ((bar_collor[0] - collor_coef),
                      (bar_collor[1] + collor_coef), bar_collor[2])

    screen.fill(black)
    fontObj = pygame.font.Font('freesansbold.ttf', text_size)
    textSurfaceObj = fontObj.render(str(bar_weidth // 4) + '%', True, white)
    textRectObj = textSurfaceObj.get_rect()
    textRectObj.center = (x_coordinate + (max_size // 2), y_coordinate + ((bar_height - text_size // 15) // 2))
    pygame.draw.rect(screen, bar_collor, (x_coordinate, y_coordinate,
                                          bar_weidth, bar_height))
    screen.blit(textSurfaceObj, textRectObj)
    pygame.display.update()
    pygame.display.flip()

pygame.quit()
