import pygame

# STATIC VARIABLES
WEIDTH = 512
HEIGHT = 512
FPS = 50
# COLORS
aqua = (0, 255, 255)  # морская волна
black = (0, 0, 0)  # черный
blue = (0, 0, 255)  # синий
fuchsia = (255, 0, 255)  # фуксия
gray = (128, 128, 128)  # серый
green = (0, 128, 0)  # зеленый
lime = (0, 255, 0)  # цвет лайма
maroon = (128, 0, 0)  # темно-бордовый
navy_blue = (0, 0, 128)  # темно-синий
olive = (128, 128, 0)  # оливковый
purple = (128, 0, 128)  # фиолетовый
red = (255, 0, 0)  # красный
silver = (192, 192, 192)  # серебряный
teal = (0, 128, 128)  # зелено-голубой
white = (255, 255, 255)  # белый
yellow = (255, 255, 0)  # желтый


pygame.init()


# CREATE GAME START FUNCTIONS
screen = pygame.display.set_mode((WEIDTH, HEIGHT))
pygame.display.set_caption("Clocks")
clock = pygame.time.Clock()
running = True

#surface
width = 70
height = 70
clock_image = pygame.image.load("pngegg.png").convert()

clock_surface = pygame.transform.scale(clock_image, (width, height))
clock_rect = clock_surface.get_rect(center=(256, 256))


def rotade(image, rect):
    rot_image = pygame.transform.rotate(image, 45)
    rot_rect = rot_image.get_rect(center=rect.center)
    return rot_image, rot_rect


while running:
    clock.tick(FPS)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    button = pygame.key.get_pressed()
    rotated_clock = clock_surface
    screen.fill(black)
    screen.blit(clock_surface, clock_rect)
    if button[pygame.K_LEFT]:
        ans = rotade(clock_image, clock_rect)
        clock_surface, clock_rect = pygame.transform.scale(ans[0].convert(), (width, height)), clock_rect
        clock_image = ans[0]
        screen.blit(clock_surface, clock_rect)
    if button[pygame.K_RIGHT]:
        ans = rotade(clock_image, clock_rect)
        clock_surface, clock_rect = pygame.transform.scale(ans[0].convert(), (width, height)), clock_rect
        clock_image = ans[0]
        screen.blit(clock_surface, clock_rect)
    pygame.display.update()
    pygame.display.flip()

pygame.quit()

