import pygame, random

# STATIC VARIABLES
WEIDTH = 512
HEIGHT = 512
FPS = 50
# COLORS
aqua = (0, 255, 255)  # морская волна
black = (0, 0, 0)  # черный
blue = (0, 0, 255)  # синий
fuchsia = (255, 0, 255)  # фуксия
gray = (128, 128, 128)  # серый
green = (0, 128, 0)  # зеленый
lime = (0, 255, 0)  # цвет лайма
maroon = (128, 0, 0)  # темно-бордовый
navy_blue = (0, 0, 128)  # темно-синий
olive = (128, 128, 0)  # оливковый
purple = (128, 0, 128)  # фиолетовый
red = (255, 0, 0)  # красный
silver = (192, 192, 192)  # серебряный
teal = (0, 128, 128)  # зелено-голубой
white = (255, 255, 255)  # белый
yellow = (255, 255, 0)  # желтый


pygame.init()


# CREATE GAME START FUNCTIONS
pygame.display.set_caption("Tanki")
screen = pygame.display.set_mode((WEIDTH, HEIGHT))
clock = pygame.time.Clock()
running = True

#CREATE TANK VARIABLES
width = 70
height = 70
x_coordinate = 256
y_coordinate = 500 - height//2
speed = 5
tank = pygame.transform.scale(pygame.image.load("Tank.png").convert(), (width, height))
tank_rect = tank.get_rect(center=(x_coordinate, y_coordinate))

balls = []
ball_weidth = 10
ball_height = 5
ball_speed = speed * 2
ball_surface = pygame.transform.scale(pygame.image.load("Bullet.png").convert(), (ball_height, ball_weidth))

SPAWNTANK = pygame.USEREVENT
pygame.time.set_timer(SPAWNTANK, 3000)
tanks_surface = pygame.transform.scale(pygame.image.load("tankbody.png").convert(), (width//2, height//2))

tanks = []


def create_tank():
    return tanks_surface.get_rect(center=(random.randint(width//2, WEIDTH), random.randint(height//2, HEIGHT//2)))


def check_collision(object, elements):
    for elem in elements:
        if elem.colliderect(object):
            return False, elem
    return True, None


while running:
    clock.tick(FPS)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                balls.append(ball_surface.get_rect(center=(x_coordinate, y_coordinate - width // 2)))
        if event.type == SPAWNTANK:
            tanks.append(create_tank())


    button = pygame.key.get_pressed()
    if button[pygame.K_LEFT] and x_coordinate > 0 + width//3:
        x_coordinate -= speed
    if button[pygame.K_RIGHT] and x_coordinate < 512 - width//3:
        x_coordinate += speed
    if button[pygame.K_UP] and y_coordinate > 256 + height//2:
        y_coordinate -= speed
    if button[pygame.K_DOWN] and y_coordinate < 512 - height//2:
        y_coordinate += speed

    screen.fill(black)

    for ball in balls:
        if ball.center[1] > 0:
            ball.center = (ball.center[0], ball.center[1] - ball_speed)
            screen.blit(ball_surface, ball)
        else:
            balls.remove(ball)

    for tanked in tanks:
        ans = check_collision(tanked, balls)
        if ans[0] is True:
            screen.blit(tanks_surface, tanked)
        else:
            tanks.remove(tanked)
            balls.remove(ans[1])
    tank_rect.center = (x_coordinate, y_coordinate)
    screen.blit(tank, tank_rect)
    pygame.display.update()
    pygame.display.flip()

pygame.quit()