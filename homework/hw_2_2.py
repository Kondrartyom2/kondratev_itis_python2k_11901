def binary(f):
    def convert(number: int):
        num = f(number)
        answer = ''
        while num > 0:
            answer += str(num % 2)
            num = num // 2
        return answer[::-1]

    return convert


@binary
def my(s: int):
    return s


print(my(int(input())))
