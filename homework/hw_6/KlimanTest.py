key = {1: ['3A', '5B', '8A', '9B', '10A', '13B', '14B', '16B', '17A', '22B', '25A', '28A'],
         2: ['2B', '5A', '8B', '11A', '14A', '19A', '20A', '21B', '23A', '26B', '28B', '30B'],
         3: ['2A', '4A', '7B', '10B', '12B', '13A', '18B', '20B', '22A', '24B', '26A', '29A'],
         4: ['1A', '5B', '6A', '7A', '9A', '12A', '15B', '17B', '19B', '23B', '27A', '29B'],
         5: ['1B', '3B', '4B', '11B', '15A', '16A', '18A', '21A', '24A', '25B', '27B', '30A']}
tipaz = {1: 'Соперничество', 2: 'Сотрудничество', 3: 'Компромисс', 4: 'Избегание', 5: 'Приспособление'}
answer = [0, 0, 0, 0, 0]
cur_question = 1
file = open('questions.txt', 'r', encoding='cp1251')
for line in file:
    print(line)
    if line.startswith('Б)'):
        chose = input('Your chose: ')
        for keys in key.keys():
            if str(cur_question) + chose.strip() in key[keys]:
                answer[keys - 1] += 1
        cur_question += 1
for i in range(5):
    print('{}: {}'.format(tipaz[i + 1], answer[i]))