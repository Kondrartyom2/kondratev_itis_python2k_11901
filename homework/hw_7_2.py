from datetime import datetime
import bs4
import requests

style_forecast = ['link', 'link_theme_normal', 'i-bem']


def forecast(div):
    time = ''
    temp = ''
    for span in div.select('span'):
        if span:
            if (span.get('class') == ['temp__pre']) or (span.get('class') == ['temp__pre-a11y', 'a11y-hidden']) :
                time = span.get_text()
            elif span.get('class') == ['temp__value']:
                temp = span.get_text()
    print(time, end=' ')
    print(temp)


try:
    day = datetime.now().day
    response = requests.get('https://yandex.ru/pogoda/kazan')
    for elements in bs4.BeautifulSoup(response.text, 'html.parser').select('a'):
        if elements.get('class') == style_forecast:
            if elements.get('href').split('#')[-1] == str(day):
                for divs in elements.select('div'):
                    if divs.get('class') == ['forecast-briefly__name']:
                        print(divs.get_text())
                    if divs.get('class') == ['temp', 'forecast-briefly__temp', 'forecast-briefly__temp_day']:
                        forecast(divs)
                    if divs.get('class') == ['temp', 'forecast-briefly__temp', 'forecast-briefly__temp_night']:
                        forecast(divs)
                day += 1
        if day > (datetime.now().day + 7):
            break

except requests.exceptions.ConnectionError:
    print("Ошибка")
