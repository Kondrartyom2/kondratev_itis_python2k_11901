import pygame
import sys
import random


def draw_floor():
    screen.blit(floor_surface, (floor_x_pos, 450 * size_var))
    screen.blit(floor_surface, (floor_x_pos + 288 * size_var, 450 * size_var))


def create_pipe():
    random_pipe_pos = random.choice(pipe_height)
    bottom_pipe = pipe_surface.get_rect(midtop=(350 * size_var, random_pipe_pos))
    top_pipe = pipe_surface.get_rect(midbottom=(350 * size_var, random_pipe_pos - 150 * size_var))
    return bottom_pipe, top_pipe


def bird_animation():
    new_bird = bird_frames[bird_index]
    new_bird_rect = new_bird.get_rect(center=(50 * size_var, bird_rect.centery))
    return new_bird, new_bird_rect


def rotate_bird(bird):
    new_bird = pygame.transform.rotozoom(bird, -bird_movement * (5 // size_var), 1)
    return new_bird


def check_collision(pipes):
    for pipe in pipes:
        if bird_rect.colliderect(pipe):
            death_sound.play()
            return False

    if bird_rect.top <= -50 * size_var or bird_rect.bottom >= 450 * size_var:
        return False

    return True


def move_pipes(pipes):
    for pipe in pipes:
        pipe.centerx -= 2.5 * size_var
    if len(pipes) > 0:
        update_score(pipes[len(pipes) - 1])
    return pipes


def update_score(last_pipe):
    global score
    if last_pipe.x == 0:
        score += 1


def draw_pipes(pipes):
    for pipe in pipes:
        if pipe.bottom >= 512 * size_var:
            screen.blit(pipe_surface, pipe)
        else:
            flip_pipe = pygame.transform.flip(pipe_surface, False, True)
            screen.blit(flip_pipe, pipe)



size_var = 1

#Variables
WIDTH = 288 * size_var
HEIGHT = 512 * size_var

#pygame inited
pygame.mixer.pre_init(frequency=44100, size=16, channels=1)
pygame.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
clock = pygame.time.Clock()
game_font = pygame.font.Font('freesansbold.ttf', 20 * size_var)


# Game Variables
gravity = 0.125 * size_var
bird_speed = 5 * size_var
bird_movement = 0
game_active = True
score = 0
high_score = 0
font = pygame.font.SysFont('Comic Sans MS', 15 * size_var)


#surfaces
bg_surface = pygame.image.load('flappy-bird-assets/sprites/background-day.png').convert()

floor_surface = pygame.image.load('flappy-bird-assets/sprites/base.png').convert()
floor_x_pos = 0

bird_downflap = pygame.image.load('flappy-bird-assets/sprites/bluebird-downflap.png').convert_alpha()
bird_midflap = pygame.image.load('flappy-bird-assets/sprites/bluebird-midflap.png').convert_alpha()
bird_upflap = pygame.image.load('flappy-bird-assets/sprites/bluebird-upflap.png').convert_alpha()
bird_frames = [bird_downflap, bird_midflap, bird_upflap]
bird_index = 0
bird_surface = bird_frames[bird_index]
bird_rect = bird_surface.get_rect(center=(50 * size_var, 256 * size_var))

BIRDFLAP = pygame.USEREVENT + 1
pygame.time.set_timer(BIRDFLAP, 100 * size_var)


pipe_surface = pygame.image.load('flappy-bird-assets/sprites/pipe-green.png')
pipe_list = []
SPAWNPIPE = pygame.USEREVENT
pygame.time.set_timer(SPAWNPIPE, 2400 // 1 * size_var)
pipe_height = [200 * size_var, 300 * size_var, 400 * size_var]

flap_sound = pygame.mixer.Sound('flappy-bird-assets/audio/wing.wav')
death_sound = pygame.mixer.Sound('flappy-bird-assets/audio/hit.wav')
score_sound = pygame.mixer.Sound('flappy-bird-assets/audio/point.wav')
score_sound_countdown = 50 * size_var


while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE and game_active:
                bird_movement = -bird_speed
                flap_sound.play()
            if event.key == pygame.K_SPACE and game_active is False:
                game_active = True
                pipe_list.clear()
                bird_rect.center = (50 * size_var, 256 * size_var)
                bird_movement = 0
                score = 0

        if event.type == SPAWNPIPE:
            pipe_list.extend(create_pipe())

        if event.type == BIRDFLAP:
            if bird_index < 2:
                bird_index += 1
            else:
                bird_index = 0

            bird_surface, bird_rect = bird_animation()

    screen.blit(bg_surface, (0, 0))

    if game_active:
        # Bird
        bird_movement += gravity
        rotated_bird = rotate_bird(bird_surface)
        bird_rect.centery += bird_movement
        screen.blit(rotated_bird, bird_rect)
        game_active = check_collision(pipe_list)

        if game_active is False:
            if high_score < score:
                high_score = score

        # Pipes
        pipe_list = move_pipes(pipe_list)
        draw_pipes(pipe_list)

        score_sound_countdown -= 1
        if score_sound_countdown <= 0:
            score_sound.play()
            score_sound_countdown = 100 * size_var


        # Score
        score_text = font.render("SCORE " + str(score), True, (255, 255, 255))
        screen.blit(score_text, score_text.get_rect(center=(WIDTH // 2, 50)))
    else:
        score_text = font.render("YOUR HIGH SCORE: " + str(high_score), True, (255, 255, 255))
        instruction = font.render("SPACE TO CONTINUE", True, (255, 255, 255))
        screen.blit(score_text, score_text.get_rect(center=(WIDTH // 2, 50 * size_var)))
        screen.blit(instruction, instruction.get_rect(center=(WIDTH // 2, HEIGHT // 2)))

    # Floor
    floor_x_pos -= 1
    draw_floor()
    if floor_x_pos <= -288 * size_var:
        floor_x_pos = 0

    pygame.display.update()
    clock.tick(120)
